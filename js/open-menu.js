
//MENU ENCUESTAS SELECCIONADAS
$(".open-menu").click(function() {
  // $(".menu-item").show();
  $(".menu-item").addClass("show-menu");
});

$(document).click(function(e) {
  if (!$(e.target).hasClass("open-menu") &&
    !$(e.target).hasClass("menu-item") &&
    $(e.target).parents(".menu-item").length === 0) {
    // $(".menu-item").hide();
    $(".menu-item").removeClass("show-menu");
  }
});



//MENU FAVORABILIDAD (primera variable)
$(".fav-open").click(function() {
  // $(".menu-item").show();
  $(".fav-graph").addClass("fav-show-menu");
});

$(document).click(function(e) {
  if (!$(e.target).hasClass("fav-open") &&
    !$(e.target).hasClass("fav-graph") &&
    $(e.target).parents(".fav-graph").length === 0) {
    // $(".menu-item").hide();
    $(".fav-graph").removeClass("fav-show-menu");
  }
});


//MENU COMPROMISO (segunda variable)
$(".com-open").click(function() {
  // $(".menu-item").show();
  $(".com-graph").addClass("com-show-menu");
});

$(document).click(function(e) {
  if (!$(e.target).hasClass("com-open") &&
    !$(e.target).hasClass("com-graph") &&
    $(e.target).parents(".com-graph").length === 0) {
    // $(".menu-item").hide();
    $(".com-graph").removeClass("com-show-menu");
  }
});


//MENU CANTIDAD DE RESPUESTAS (tercera variable)
$(".res-open").click(function() {
  // $(".menu-item").show();
  $(".res-graph").addClass("res-show-menu");
});

$(document).click(function(e) {
  if (!$(e.target).hasClass("res-open") &&
    !$(e.target).hasClass("res-graph") &&
    $(e.target).parents(".res-graph").length === 0) {
    // $(".menu-item").hide();
    $(".res-graph").removeClass("res-show-menu");
  }
});