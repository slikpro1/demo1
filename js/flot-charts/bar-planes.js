'use strict';

$(document).ready(function(){

    // Chart Data
    var barChartData = [
        {
            label: 'En progreso',
            data: [[1,60], [2,30], [3,50], [4,100]],
            color: '#2196F3',
            bars: {
                order: 0
            }
        },
        {
            label: 'Vencidas',
            data: [[1,20], [2,90], [3,60], [4,40]],
            color: '#f16b6c',
            bars: {
                order: 1
            }
        },
        {
            label: 'Finalizadas',
            data: [[1,100], [2,20], [3,60], [4,90]],
            color: '#c4c4c4',
            bars: {
                order: 2
            }
        }
    ];

    // Chart Options
    var barChartOptions = {
        series: {
            bars: {
                show: true,
                barWidth: 0.05,
                fill: 1
            }
        },
        grid : {
            borderWidth: 1,
            borderColor: '#f8f8f8',
            show : true,
            hoverable : true,
            clickable : true
        },
        yaxis: {
            tickColor: '#f8f8f8',
            tickDecimals: 0,
            font :{
                lineHeight: 13,
                style: "normal",
                color: "#252525",
            },
            shadowSize: 0
        },
        xaxis: {
            tickColor: '#fff',
            tickDecimals: 0,
            font :{
                lineHeight: 20,
                style: "normal",
                color: "#252525"
            },
            shadowSize: 0,
            ticks: [
                        [1, "Diversidad"],
                        [2, "Bienestar"],
                        [3, "Cultura"],
                        [4, "Compromiso"]
                    ],
        },
        legend:{
            container: '.flot-chart-legends--bar',
            backgroundOpacity: 0.5,
            noColumns: 0,
            backgroundColor: '#fff',
            lineWidth: 0,
            labelBoxBorderColor: '#fff'
        }
    };

    // Create chart
    if ($('.flot-bar')[0]) {
        $.plot($('.flot-bar'), barChartData, barChartOptions);
    }
});
