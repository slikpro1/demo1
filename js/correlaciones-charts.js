function start()
{
func1();
func2();

}

function func1() {

//Better to construct options first and then pass it as a parameter
var options = {
	animationEnabled: false,
	zoomEnabled: true,
	colorSet: "colorSet2",
	axisX: {
		// title:"Turnover",
		valueFormatString: "#####",
		crosshair: {
			enabled: true,
			snapToDataPoint: true
		}
	},
	axisY:{
		// title: "Cultural Fit",
		gridThickness: 0,
		valueFormatString: "#####",
		crosshair: {
			enabled: true,
			snapToDataPoint: true
		}
	},
	data: [{
		type: "scatter",
		toolTipContent: "<b>Turnover: </b>{x}<br/><b>Cultural Fit: </b>{y}",
		dataPoints: [
			{ x: 3.8, y: 10},
			{ x: 4, y: 9.6},
			{ x: 4.2, y: 9.4},
			{ x: 5, y: 9},
			{ x: 5.2, y: 8.7},
			{ x: 5.9, y: 8.3},
			{ x: 6, y: 8},
			{ x: 6.1, y: 8.1},
			{ x: 6.5, y: 7.9},
			{ x: 6.9, y: 7.2},
			{ x: 7.1, y: 7.5},
			{ x: 7.3, y: 7.1},
			{ x: 7.7, y: 7},
			{ x: 7.9, y: 6.5},
			{ x: 8, y: 6.2},
			{ x: 8.2, y: 6},
			{ x: 8.7, y: 5.9},
		]
	}]
};

$("#chart-scatter").CanvasJSChart(options);

}



// predictive Chart

function func2() {

var options = {
	animationEnabled: true,
	axisX: {
		title: "Turnover",
		valueFormatString: "#####",
	},
	axisY: {
		title: "Cultural Fit",
		valueFormatString: "#####",
	},
	data: [{
		type: "spline",
		toolTipContent: "<b>Turnover: </b>{x}<br/><b>Cultural Fit: </b>{y}",
		dataPoints: [
			{ x: 2, y: 9.8 },
			{ x: 2.1, y: 9.3 },
			{ x: 2.4, y: 9.0 },
			{ x: 2.7, y: 8.5 },
			{ x: 2.9, y: 8.3 },
			{ x: 3.1, y: 7.8 },
			{ x: 3.5, y: 6.2 },
			{ x: 3.6, y: 6 },
			{ x: 3.8, y: 5.1 },
			{ x: 4, y: 4 },
			{ x: 4.3, y: 3.2 },
			{ x: 5, y: 1.8 },
			{ x: 6, y: 1.5 },
		]
	}]
};
$("#predictive-chart").CanvasJSChart(options);
}

window.onload = start;

