

 $(document).on('click', '.nl-dd ul li:first-child', function (e) {
    $(".heatmap header .total .favo").removeClass('d-none');
    $(".heatmap header .total .mean").addClass('d-none');
    $('.versus select[name="encuestas"]').attr('disabled', false);
    $(".heatmap .numero small").removeClass('d-none');
    $(".heatmap .total small").removeClass('d-none');
    $(".heatmap .versus").removeClass('opacity');
});

 $(document).on('click', '.nl-dd ul li:last-child', function (e) {
    $(".heatmap header .total .favo").addClass('d-none');
    $(".heatmap header .total .mean").removeClass('d-none');
    $(".heatmap .versus").addClass('opacity');
    $('.versus select[name="encuestas"]').attr('disabled', 'disabled');
    $(".heatmap .numero small").addClass('d-none');
    $(".heatmap .total small").addClass('d-none');
});

