$('.modulo.oficina .toggle-switch__checkbox').change(function(){
    if($(this).is(":checked")) {
        $('.filtros .oficina .btn').addClass("excluir");
        $('.filtros .oficina .btn').removeClass("active");
    } else {
        $('.filtros .oficina .btn').removeClass("excluir");
        $('.filtros .oficina .btn').removeClass("active");
    }
});


$('.modulo.jefe .toggle-switch__checkbox').change(function(){
    if($(this).is(":checked")) {
        $('.filtros .jefe .select2-selection__rendered').addClass("excluir");
        $('.filtros .jefe .check .name span').addClass("excluir");
    } else {
        $('.filtros .jefe .select2-selection__rendered').removeClass("excluir");
        $('.filtros .jefe .check .name span').removeClass("excluir");
    }
});

$('.modulo.etapas .toggle-switch__checkbox').change(function(){
    if($(this).is(":checked")) {
        $('.filtros .etapas .btn').addClass("excluir");
        $('.filtros .etapas .btn').removeClass("active");
    } else {
        $('.filtros .etapas .btn').removeClass("excluir");
        $('.filtros .etapas .btn').removeClass("active");
    }
});

$('.modulo.touchpoints .toggle-switch__checkbox').change(function(){
    if($(this).is(":checked")) {
        $('.filtros .touchpoints .btn').addClass("excluir");
        $('.filtros .touchpoints .btn').removeClass("active");
    } else {
        $('.filtros .touchpoints .btn').removeClass("excluir");
        $('.filtros .touchpoints .btn').removeClass("active");
    }
});

$('.modulo.responsables .toggle-switch__checkbox').change(function(){
    if($(this).is(":checked")) {
        $('.filtros .responsables .btn').addClass("excluir");
        $('.filtros .responsables .btn').removeClass("active");
    } else {
        $('.filtros .responsables .btn').removeClass("excluir");
        $('.filtros .responsables .btn').removeClass("active");
    }
});


$('.modulo.posiciones .toggle-switch__checkbox').change(function(){
    if($(this).is(":checked")) {
        $('.filtros .posiciones .btn').addClass("excluir");
        $('.filtros .posiciones .btn').removeClass("active");
    } else {
        $('.filtros .posiciones .btn').removeClass("excluir");
        $('.filtros .posiciones .btn').removeClass("active");
    }
});

$('.modulo.tags .toggle-switch__checkbox').change(function(){
    if($(this).is(":checked")) {
        $('.filtros .tags .btn').addClass("excluir");
        $('.filtros .tags .btn').removeClass("active");
    } else {
        $('.filtros .tags .btn').removeClass("excluir");
        $('.filtros .tags .btn').removeClass("active");
    }
});


$('.deptos-checkbox .toggle-switch__checkbox').change(function(){
    if($(this).is(":checked")) {
        $('.deptos-checkbox .deptos-wrap .btn-group .btn').addClass("excluir");
        $('.deptos-checkbox .deptos-wrap .btn-group .btn').removeClass("active");
        $('.deptos-checkbox .txt-seleccionados .excluidos').removeClass("d-none");
        $('.deptos-checkbox .txt-seleccionados .seleccionados').addClass("d-none");
    } else {
        $('.deptos-checkbox .deptos-wrap .btn-group .btn').removeClass("excluir");
        $('.deptos-checkbox .deptos-wrap .btn-group .btn').removeClass("active");
        $('.deptos-checkbox .txt-seleccionados .excluidos').addClass("d-none");
        $('.deptos-checkbox .txt-seleccionados .seleccionados').removeClass("d-none");
    }
});

