
window.onload = function() {

var options = {
    exportEnabled: false,
    animationEnabled: true,
    data: [{
        type: "pie",
        showInLegend: false,
        toolTipContent: "{name}: {y} (#percent%)",
        dataPoints: [
            { y: 2465, name: "Respondieron", color: "#0057ff"},
            { y: 1278, name: "No respondieron", color: "#c4c4c4" },
            { y: 207, name: "Rechazaron", color: "#ff0000" }
        ]
    }]
};
$("#chartContainer").CanvasJSChart(options);

}