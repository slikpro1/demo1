
   var xValues = ["Respondieron", "No respondieron", "Rechazaron"];
   var yValues = [65, 25, 10];
   var barColors = [
     "#2196F3",
     "#F06B69",
     "#c4c4c4"
   ];

   new Chart("myChart", {
     type: "doughnut",
     options: {
         legend: {
            display: false
         }},
     data: {
       labels: xValues,
       datasets: [{
         backgroundColor: barColors,
         data: yValues
       }]
     },
   });