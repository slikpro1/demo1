    $(window).scroll(function() {
    if ($(this).scrollTop() > 200) {
        $('#backtotop').addClass("btn-bttop");
    } else {
        $('#backtotop').removeClass("btn-bttop");
    }
});

$("#backtotop").click(function () {
   //1 second of animation time
   //html works for FFX but not Chrome
   //body works for Chrome but not FFX
   //This strange selector seems to work universally
   $("html, body").animate({scrollTop: 0}, 1000);
});