function rangeSlideSize(value) {
	document.getElementById('rangeValueSize').innerHTML = formatNumber(value);
	updateCalculations();
}

function rangeSlideSalary(value) {
	document.getElementById('rangeValueSalary').innerHTML = formatNumber(value);
	updateCalculations();
}

function rangeSlideTurnover(value) {
	document.getElementById('rangeValueTurnover').innerHTML = value;
	updateCalculations();
}


function formatNumber(num) {
  return Math.trunc(num).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

function updateCalculations() {
	const companySize = $("#companySizeValue").val();
	const averageSalary = $("#averageSalaryValue").val();
	const turnoverRate = $("#turnoverRateValue").val();

	// console.log("companySize: " + companySize);
	// console.log("averageSalary: " + averageSalary);
	// console.log("turnoverRate: " + turnoverRate);

	// PRODUCTIVITY SAVINGS

	const BENEFITS_COSTS = 141;
	const PRODUCTIVITY_GAINS = 17;

	const productivitySavings = (((companySize * averageSalary * BENEFITS_COSTS) / 100 ) * PRODUCTIVITY_GAINS) / 100;

	// console.log("productivitySavings: " + productivitySavings);
	$("#productiviySavingsValue").html(formatNumber(productivitySavings));

	// ABSENTEEISM SAVINGS

	const WORKDAYS_PER_YEAR = 20 * 12;
	const LESS_DAYS_OF_ABSTENTEEISM = 5;

	const absenteeismSavings = (((companySize * averageSalary * BENEFITS_COSTS) / 100 ) / WORKDAYS_PER_YEAR) * LESS_DAYS_OF_ABSTENTEEISM;
	// console.log("absenteeismSavings: " + absenteeismSavings);
	$("#absenteeismSavingsValue").html(formatNumber(absenteeismSavings));

	// TURNOVER SAVINGS

	const TURNOVER_COST = 85;
	const TURNOVER_RATE_REDUCTION = turnoverRate >= 15 ? 59 : 24;

	const turnoverSavings = (((((((averageSalary * BENEFITS_COSTS) / 100) * companySize * TURNOVER_COST) / 100) * turnoverRate) / 100) * TURNOVER_RATE_REDUCTION) / 100
	// console.log("turnoverSavings: " + turnoverSavings);
	$("#turnoverSavingsValue").html(formatNumber(turnoverSavings));

	// ONBOARDING SAVINGS

	// const DAYS_OF_ONBOARDING = 90;
	// const INCREMENT_OF_VALUE = 15;

	// const savedPerEmployeeOnboarding = ((((averageSalary * BENEFITS_COSTS) / 100) / WORKDAYS_PER_YEAR) * DAYS_OF_ONBOARDING * INCREMENT_OF_VALUE) / 100;
	// console.log("savedPerEmployeeOnboarding: " + savedPerEmployeeOnboarding);

	// const EMPLOYEE_COUNT_GROWTH = 10;

	// const onboardingDueToGrowth = (companySize * EMPLOYEE_COUNT_GROWTH) / 100;
	// console.log("onboardingDueToGrowth: " + onboardingDueToGrowth);

	// const CURRENT_TURNOVER_RATE = 25;

	// const onboardingDueToTurnovers = (companySize * CURRENT_TURNOVER_RATE) / 100;
	// console.log("onboardingDueToTurnovers: " + onboardingDueToTurnovers);

	// const onboardingSavings = savedPerEmployeeOnboarding * (onboardingDueToGrowth + onboardingDueToTurnovers);
	// console.log("onboardingSavings: " + onboardingSavings);
	// $("#onboardingSavingsValue").html(formatNumber(onboardingSavings));

	const totalSavings = productivitySavings + absenteeismSavings + turnoverSavings;
	// console.log("total: " + totalSavings);
	$("#totalSavingsValue").html(formatNumber(totalSavings));

}
