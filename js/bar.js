'use strict';

$(document).ready(function(){

    // Chart Data
    var barChartData = [
        {
            label: 'Mujeres',
            data: [[1,40], [2,30], [3,50], [4,65], [5,10], [6,70], [7,85]],
            color: 'red',
            bars: {
                order: 0
            }
        },
        {
            label: 'Hombres',
            data: [[1,50], [2,60], [3,40], [4,40], [5,45], [6,55], [7,65]],
            color: '#0057ff',
            bars: {
                order: 1
            }
        },
        {
            label: 'Otro',
            data: [[1,8], [2,7], [3,12], [4,2], [5,5], [6,8], [7,11]],
            color: '#c4c4c4',
            bars: {
                order: 2
            }
        }
    ];


    // Chart Options
    var barChartOptions = {
        series: {
            bars: {
                show: true,
                barWidth: 0.05,
                fill: 1
            }
        },
        grid : {
            borderWidth: 1,
            borderColor: '#f8f8f8',
            show : true,
            hoverable : true,
            clickable : true
        },
        yaxis: {
            tickColor: '#f8f8f8',
            tickDecimals: 0,
            font :{
                lineHeight: 13,
                style: "normal",
                color: "#9f9f9f",
            },
            shadowSize: 0
        },
        xaxis: {
            tickColor: '#fff',
            tickDecimals: 0,
            font :{
                lineHeight: 13,
                style: "normal",
                color: "#9f9f9f"
            },
            shadowSize: 0,
        },
        legend:{
            container: '.flot-chart-legends--bar',
            backgroundOpacity: 0.5,
            noColumns: 0,
            backgroundColor: '#fff',
            lineWidth: 0,
            labelBoxBorderColor: '#fff'
        }
    };

    // Create chart
    if ($('.flot-bar')[0]) {
        $.plot($('.flot-bar'), barChartData, barChartOptions);
    }
});
