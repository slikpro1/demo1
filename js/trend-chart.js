window.onload = function () {

var options = {
	animationEnabled: true,
	theme: "light2",
	// title: {
	// 	text: "Monthly Sales Data"
	// },
	axisX: {
		valueFormatString: "MMM",
		labelFontFamily: "proxima_nova_alt",
		labelMaxWidth: 100,
		labelTextAlign: "center",
		margin: 20
	},
	axisY: {
		suffix: " %",
		labelFormatter: addSymbols,
		gridColor: "#efefef",
		labelFontFamily: "proxima_nova_alt"
	},

	toolTip: {
		shared: true,
		fontFamily: "proxima_nova_alt",
		padding: 60
	},
	legend: {
		cursor: "pointer",
		itemclick: toggleDataSeries,
		horizontalAlign: "center", // "left" , "right"
       	verticalAlign: "bottom",  // "top" , "bottom"
       	fontSize: 12,
       	markerMargin: 8,
       	itemWidth: 140,
	},



	dataPointWidth: 80,  // Ancho de las columnas
	data: [

		{	
			// legendMarkerType: "circle",
			type: "splineArea",
			name: "Compromiso",
			color: "#fee1b5",
			// lineColor: "#3a7769",
			fillOpacity: 1,
			markerBorderColor: "#fee1b5",
			markerBorderThickness: 2,
			markerColor: "#ffffff",
			legendMarkerColor: "#fee1b5",
			showInLegend: true,
			yValueFormatString: "# '%'",
			dataPoints: [
				{label: "Pulso Julio 2020", y: 72,},
      			{label: "Pulso Agosto 2020", y: 75,},
      			{label: "Pulso Septiembre 2020", y: 70,},
      			{label: "Pulso Octubre 2020", y: 83,},
      			{label: "Pulso Noviembre 2020", y: 78,},
      			{label: "Pulso Diciembre 2020", y: 88,},
      			{label: "Pulso Enero 2021", y: 95,},
				]
			},

		{	
			type: "column",
			name: "Favorabilidad",
			color: "#98c1f9",
			showInLegend: true,
			xValueFormatString: "MMMM YYYY",
			yValueFormatString: "# '%'",
			dataPoints: [
				{label: "Pulso Julio 2020", y: 70},
			    {label: "Pulso Agosto 2020", y: 73},
			    {label: "Pulso Septiembre 2020", y: 68},
			    {label: "Pulso Octubre 2020", y: 75},
			    {label: "Pulso Noviembre 2020", y: 80},
			    {label: "Pulso Diciembre 2020", y: 84},
			    {label: "Pulso Enero 2021", y: 92},
				]
			},
		
		{	
			type: "spline",
			name: "Participación",
			markerBorderThickness: 2,
			markerBorderColor: "#25c986",
			lineColor: "#25c986",
			markerColor: "#ffffff",
			legendMarkerColor: "#25c986",
			// legendlineColor: "#25c986",
			showInLegend: true,
			yValueFormatString: "#",
			dataPoints: [
				{label: "Pulso Julio 2020", y: 75},
			    {label: "Pulso Agosto 2020", y: 73},
			    {label: "Pulso Septiembre 2020", y: 78},
			    {label: "Pulso Octubre 2020", y: 79},
			    {label: "Pulso Noviembre 2020", y: 85},
			    {label: "Pulso Diciembre 2020", y: 83},
			    {label: "Pulso Enero 2021", y: 92},
				]
			}
		]
};
$("#chartContainer").CanvasJSChart(options);

function addSymbols(e) {
	var suffixes = ["", "K", "M", "B"];
	var order = Math.max(Math.floor(Math.log(e.value) / Math.log(1000)), 0);

	if (order > suffixes.length - 1)
		order = suffixes.length - 1;

	var suffix = suffixes[order];
	return CanvasJS.formatNumber(e.value / Math.pow(1000, order)) + suffix;
}

function toggleDataSeries(e) {
	if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	} else {
		e.dataSeries.visible = true;
	}
	e.chart.render();
}


}


