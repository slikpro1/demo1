
// clic boton enlace publico
 $(document).on('click', '.audiencia .seleccionar .enlace .btn', function (e) {
    $(".enlace").addClass('fadeOutLeft');
    $(".base").addClass('fadeOutRight');
    $(".title-page").addClass('fadeOutUp');
    setTimeout(function(){
        $(".enlace").addClass('d-none');
        $(".base").addClass('d-none');
        $(".seleccionar").addClass('d-none');
        $(".enlace-content").removeClass('d-none fadeOut');
        $(".enlace-content").addClass('fadeIn');
     },150);
});


// clic boton Base de datos
 $(document).on('click', '.audiencia .seleccionar .base .btn', function (e) {
    $(".enlace").addClass('fadeOutLeft');
    $(".base").addClass('fadeOutRight');
    $(".title-page").addClass('fadeOutUp');
    setTimeout(function(){
        $(".enlace").addClass('d-none');
        $(".base").addClass('d-none');
        $(".seleccionar").addClass('d-none');
        $(".base-content").removeClass('d-none fadeOut');
        $(".base-content").addClass('fadeIn');
     },150);
});


// clic "cambiar modo"
 $(document).on('click', '.audiencia .cambiar-modo', function (e) {
    $(".enlace-content").removeClass('fadeIn');
    $(".enlace-content").addClass('fadeOut');
    $(".enlace-content").addClass('d-none');
    $(".base-content").removeClass('fadeIn');
    $(".base-content").addClass('fadeOut');
    $(".base-content").addClass('d-none');
    setTimeout(function(){
        $(".seleccionar").removeClass('d-none');        
        $(".enlace").removeClass('d-none fadeOutLeft');
        $(".base").removeClass('d-none fadeOutRight');
        $(".title-page").removeClass('d-none fadeOutUp');
        $(".title-page").addClass('fadeInDown');
        $(".enlace").addClass('fadeInLeft');
        $(".base").addClass('fadeInRight');
     },150);

    
    
});