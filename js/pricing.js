
$previousValue = 1000;

/**
 * @param num The number to round
 * @param precision The number of decimal places to preserve
 */
function roundUp(num, precision) {
  num = parseFloat(num);
  if (!precision) return num;
  return (Math.round(num / precision) * precision);
}

function formatNumber(num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
}

$( document ).ready(function()
{

  $("#cantidadEmpleados").on("input", function(evt) {
     value = $(this).val();
     if ($.isNumeric(value))
     {
        $previousValue = value;
        value = parseInt(value);

        calculatedValue = (-1.552*Math.log(value)+17.981).toFixed(2);
        starter = calculatedValue < 2 ? 2 : calculatedValue;
        roundUpStart = starter*value;
        totalStarter = 1500;
        if (roundUpStart > 1500) totalStarter = roundUp(roundUpStart, 10).toFixed(0);
        totalStarterPerEmployee = (totalStarter/value).toFixed(2);
        $("#starterPerEmployee").text(totalStarterPerEmployee.replace(".",","));
        $("#starterPerEmployee2").text(totalStarterPerEmployee.replace(".",","));
        $("#starterTotal").text(formatNumber(totalStarter));

        calculatedValue = (-2.603*Math.log(value)+29.113).toFixed(2);
        pro = calculatedValue < 3 ? 3 : calculatedValue;
        roundUpPro = pro*value;
        totalPro = 2300;
        if (roundUpPro > 2300) totalPro = roundUp(roundUpPro, 10).toFixed(0);
        totalProPerEmployee = (totalPro/value).toFixed(2);
        $("#proPerEmployee").text(totalProPerEmployee.replace(".",","));
        $("#proPerEmployee2").text(totalProPerEmployee.replace(".",","));
        $("#proTotal").text(formatNumber(totalPro));

        calculatedValue = (-3.695*Math.log(value)+43.15).toFixed(2);
        premium = calculatedValue < 5 ? 5 : calculatedValue;
        roundUpPremium = premium*value;
        totalPremium = 12000;
        if (roundUpPremium > 12000) totalPremium = roundUp(roundUpPremium, 10).toFixed(0);
        totalPremiumPerEmployee = (totalPremium/value).toFixed(2);
        $("#premiumPerEmployee").text(totalPremiumPerEmployee.replace(".",","));
        $("#premiumPerEmployee2").text(totalPremiumPerEmployee.replace(".",","));
        $("#premiumTotal").text(formatNumber(totalPremium));
     }
     else if (value != "") $("#cantidadEmpleados").val($previousValue);
  });
});
