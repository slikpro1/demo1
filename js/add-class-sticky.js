

// CREAR ENCUESTA 1
var observer = new IntersectionObserver(function(entries) {
  // no intersection with screen
  if(entries[0].intersectionRatio === 0)
    document.querySelector(".menu-tab").classList.add("sticky");
  // fully intersects with screen
  else if(entries[0].intersectionRatio === 1)
    document.querySelector(".menu-tab").classList.remove("sticky");
}, { threshold: [0,1] });

observer.observe(document.querySelector(".menu-tab-top"));



// Al hacer clic el Tab se posiciona a 82px del TOP EN "CREAR ENCUESTA 1"

$( ".menu-tab .nav-link" ).click(function() {
   //user clicked on the li
   scrollTo(0,82);
});


