document.addEventListener('DOMContentLoaded', function () {
        var dropdowns = document.querySelectorAll('.dropdown');

        dropdowns.forEach(function(dropdown) {
            var label = dropdown.querySelector('.label-thead');
            var items = dropdown.querySelectorAll('.dropdown-item');

            items.forEach(function(item) {
                item.addEventListener('click', function(event) {
                    event.preventDefault(); // Evita el comportamiento predeterminado del enlace
                    var title = this.querySelector('.title').textContent;
                    label.textContent = title;
                });
            });
        });
    });



$(function() {
    // Agregamos un evento para seleccionar una opción al hacer clic
    $('#benchmark1 a').click(function() {
        var selectedValue = $(this).data('value'); // Obtener el valor de data-value del span clicado

        // Guardar el valor seleccionado en el div principal
        $('#benchmark1').data('selected-value', selectedValue);
        
        // Ejecutar la lógica según el valor seleccionado
        if (selectedValue == '0') {
            $('.bench-value-1.unselect').show();
            $('.bench-value-1.bench-latam').hide();
            $('.bench-value-1.bench-mexico').hide();
            $('.bench-value-1.bench-consumo').hide();
            $('.bench-value-1.bench-top25').hide();
            $('.bench-value-1.bench-latam-diff').hide();
            $('.bench-value-1.bench-mexico-diff').hide();
            $('.bench-value-1.bench-consumo-diff').hide();
            $('.bench-value-1.bench-top25-diff').hide();
        } else if (selectedValue == '1') {
            $('.bench-value-1.unselect').hide();
            $('.bench-value-1.bench-latam').show();
            $('.bench-value-1.bench-mexico').hide();
            $('.bench-value-1.bench-consumo').hide();
            $('.bench-value-1.bench-top25').hide();
            $('.bench-value-1.bench-latam-diff').show();
            $('.bench-value-1.bench-mexico-diff').hide();
            $('.bench-value-1.bench-consumo-diff').hide();
            $('.bench-value-1.bench-top25-diff').hide();
        } else if (selectedValue == '2') {
            $('.bench-value-1.unselect').hide();
            $('.bench-value-1.bench-latam').hide();
            $('.bench-value-1.bench-latam-diff').hide();
            $('.bench-value-1.bench-mexico').show();
            $('.bench-value-1.bench-consumo').hide();
            $('.bench-value-1.bench-top25').hide();
            $('.bench-value-1.bench-latam-diff').hide();
            $('.bench-value-1.bench-mexico-diff').show();
            $('.bench-value-1.bench-consumo-diff').hide();
            $('.bench-value-1.bench-top25-diff').hide();
        } else if (selectedValue == '3') {
            $('.bench-value-1.unselect').hide();
            $('.bench-value-1.bench-latam').hide();
            $('.bench-value-1.bench-latam-diff').hide();
            $('.bench-value-1.bench-mexico').hide();
            $('.bench-value-1.bench-consumo').show();
            $('.bench-value-1.bench-top25').hide();
            $('.bench-value-1.bench-latam-diff').hide();
            $('.bench-value-1.bench-mexico-diff').hide();
            $('.bench-value-1.bench-consumo-diff').show();
            $('.bench-value-1.bench-top25-diff').hide();
        } else if (selectedValue == '4') {
            $('.bench-value-1.unselect').hide();
            $('.bench-value-1.bench-latam').hide();
            $('.bench-value-1.bench-latam-diff').hide();
            $('.bench-value-1.bench-mexico').hide();
            $('.bench-value-1.bench-consumo').hide();
            $('.bench-value-1.bench-top25').show();
            $('.bench-value-1.bench-latam-diff').hide();
            $('.bench-value-1.bench-mexico-diff').hide();
            $('.bench-value-1.bench-consumo-diff').hide();
            $('.bench-value-1.bench-top25-diff').show();
        }
    });

//     // Agregamos un evento para seleccionar una opción al hacer clic
//     $('#benchmark2 a').click(function() {
//         var selectedValue = $(this).data('value'); // Obtener el valor de data-value del span clicado

//         // Guardar el valor seleccionado en el div principal
//         $('#benchmark2').data('selected-value', selectedValue);
        
//         // Ejecutar la lógica según el valor seleccionado
//         if (selectedValue == '0') {
//             $('.bench-value-2.unselect').show();
//             $('.bench-value-2.bench-latam').hide();
//             $('.bench-value-2.bench-mexico').hide();
//             $('.bench-value-2.bench-consumo').hide();
//             $('.bench-value-2.bench-top25').hide();
//         } else if (selectedValue == '1') {
//             $('.bench-value-2.unselect').hide();
//             $('.bench-value-2.bench-latam').show();
//             $('.bench-value-2.bench-mexico').hide();
//             $('.bench-value-2.bench-consumo').hide();
//             $('.bench-value-2.bench-top25').hide();
//         } else if (selectedValue == '2') {
//             $('.bench-value-2.unselect').hide();
//             $('.bench-value-2.bench-latam').hide();
//             $('.bench-value-2.bench-mexico').show();
//             $('.bench-value-2.bench-consumo').hide();
//             $('.bench-value-2.bench-top25').hide();
//         } else if (selectedValue == '3') {
//             $('.bench-value-2.unselect').hide();
//             $('.bench-value-2.bench-latam').hide();
//             $('.bench-value-2.bench-mexico').hide();
//             $('.bench-value-2.bench-consumo').show();
//             $('.bench-value-2.bench-top25').hide();
//         } else if (selectedValue == '4') {
//             $('.bench-value-2.unselect').hide();
//             $('.bench-value-2.bench-latam').hide();
//             $('.bench-value-2.bench-mexico').hide();
//             $('.bench-value-2.bench-consumo').hide();
//             $('.bench-value-2.bench-top25').show();
//         }
//     });
 });






