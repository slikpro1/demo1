
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	// exportEnabled: true, 

	toolTip: {
		fontFamily: "proxima_nova_alt",
		backgroundColor: "#ffffff",
		cornerRadius: 10,
		backgroundColor: "#617d8a",
		borderColor: "#617d8a",
		fontColor: "#ffffff",
	},

	axisY:{
		gridColor: "#efefef",
		lineColor: "#cccccc",
		tickColor: "#cccccc",
		labelFontFamily: "proxima_nova_alt",
		labelFontSize: 14,
	},
	axisX:{
		labelFontFamily: "proxima_nova_alt",
		labelFontSize: 15
	},

	
	data: [{        
		type: "spline",
		name: "Compromiso",
		toolTipContent: "<table><tbody><tr><td style='text-align:center;padding:10px;font-size:17px;font-weight:500;'>{label}</td></tr><tr><td style='text-align:center;padding-left:15px;padding-right:15px;padding-bottom:15px;'>{name}: {y} %</td></tr></tbody></table>",
		markerSize: 12,
		lineColor: "#d3165e",
		markerColor: "#ffffff",
		markerBorderColor: "#d3165e",
		markerBorderThickness: 2,
		legendMarkerBorderColor: "#d3165e",
		legendMarkerBorderThickness: 2,
		dataPoints: [
			{ label: "Selección" , y: 88 },     
			{ label:"Onboarding", y: 60 },     
			{ label: "Crecimiento", y: 85 },     
			{ label: "Salida", y: 92 }
		]
	}, 
	{        
		type: "spline",
		name: "Participación",
		toolTipContent: "<table><tbody><tr><td style='text-align:center;padding:10px;font-size:17px;font-weight:500;'>{label}</td></tr><tr><td style='text-align:center;padding-left:15px;padding-right:15px;padding-bottom:15px;'>{name}: {y} %</td></tr></tbody></table>",
		markerSize: 12,
		lineColor: "#488bf7",
		markerColor: "#ffffff",
		markerBorderColor: "#488bf7",
		markerBorderThickness: 2,
		dataPoints: [
			{ label: "Selección" , y: 60 },     
			{ label:"Onboarding", y: 75 },     
			{ label: "Crecimiento", y: 76 },     
			{ label: "Salida", y: 30 }
		]
	},
	{        
		type: "spline",  
		name: "Favorabilidad",
		toolTipContent: "<table><tbody><tr><td style='text-align:center;padding:10px;font-size:17px;font-weight:500;'>{label}</td></tr><tr><td style='text-align:center;padding-left:15px;padding-right:15px;padding-bottom:15px;'>{name}: {y} %</td></tr></tbody></table>",
		markerSize: 12,
		lineColor: "#32c787",
		markerColor: "#ffffff",
		markerBorderColor: "#32c787",
		markerBorderThickness: 2,
		dataPoints: [
			{ label: "Selección" , y: 55 },     
			{ label:"Onboarding", y: 43 },     
			{ label: "Crecimiento", y: 56 },     
			{ label: "Salida", y: 75 }
		]
	},
	{        
		type: "spline",  
		name: "MEAN",
		toolTipContent: "<table><tbody><tr><td style='text-align:center;padding:10px;font-size:17px;font-weight:500;'>{label}</td></tr><tr><td style='text-align:center;padding-left:15px;padding-right:15px;padding-bottom:15px;'>{name}: {y}</td></tr></tbody></table>",
		markerSize: 12,     
		lineColor: "#ff9100",
		markerColor: "#ffffff",
		markerBorderColor: "#ff9100",
		markerBorderThickness: 2,
		dataPoints: [
			{ label: "Selección" , y: 40 },     
			{ label:"Onboarding", y: 50 },     
			{ label: "Crecimiento", y: 38 },     
			{ label: "Salida", y: 58 }
		]
	}]
});

chart.render();

function toggleDataSeries(e) {
	if(typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	}
	else {
		e.dataSeries.visible = true;            
	}
	chart.render();
}

}