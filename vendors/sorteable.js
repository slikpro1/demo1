// Sorteable entre si
$( function() {
    $( "#sortable" ).sortable({
      placeholder: "ui-state-highlight"
    });
    $( "#sortable" ).disableSelection();
  } );




// Sorteable entre distintas columnas
$( function() {
$( "ul.droptrue" ).sortable({
  connectWith: "ul"
});

$( "ul.dropfalse" ).sortable({
  connectWith: "ul",
  dropOnEmpty: false
});

$( "#sortable1, #sortable2, #sortable3" ).disableSelection();
} );